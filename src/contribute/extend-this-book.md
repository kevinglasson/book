# Contribute to this book

You can find the source for this book at our [GitLab](https://gitlab.com/veloren/book), feel free to make changes, correct errors and add more content.
