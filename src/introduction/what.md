# What is Veloren?

Veloren is a multiplayer voxel RPG written in Rust. It is inspired by games such as Cube World,
Legend of Zelda: Breath of the Wild, Dwarf Fortress and Minecraft.

![Beautiful trees and a cliff](../images/introduction.png)

Veloren is fully open-source, licensed under [GPL 3](https://www.gnu.org/licenses/gpl-3.0.en.html). It uses original graphics, musics and other
assets created by its community. Being contributor-driven, its development community
and user community is one and the same: developers, players, artists and musicians come together
to develop the game.

## What status is the project currently in?

After rewriting the engine from scratch (old game can be found [here](https://gitlab.com/veloren/game)) we're now at a stage
where multiple features are introduced quite rapidly.